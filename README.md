# OpenML dataset: Meta_Album_MD_MIX_Mini

https://www.openml.org/d/44287

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album OmniPrint-MD-mix Dataset (Mini)**
***
OmniPrint-MD-mix dataset consists of 28 240 images (128x128, RGB) from 706 categories. The images are synthesized with OmniPrint, and no further processing was done. The OmniPrint synthesis parameters are stated as follows: font size is 192, image size is 128, the strength of random perspective transformation is 0.04, left/right/top/bottom margins are all 20% of the image size, the strength of pre-rasterization elastic transformation is 0.035, random translation is activated both horizontally and vertically, rotation is within -60 and 60 degrees, horizontal shear is within -0.5 and 0.5, brightness is within 0.8333 and 1.2, contrast is within 0.8333 and 1.2, color enhancement is within 0.8333 and 1.2. The other parameters vary between images. We designed 20 settings, each setting is used to synthesize 2 images. All images/textures consists of photos taken by a personal mobile phone.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/MD_MIX.png)

**Meta Album ID**: OCR.MD_MIX  
**Meta Album URL**: [https://meta-album.github.io/datasets/MD_MIX.html](https://meta-album.github.io/datasets/MD_MIX.html)  
**Domain ID**: OCR  
**Domain Name**: Optical Character Recognition  
**Dataset ID**: MD_MIX  
**Dataset Name**: OmniPrint-MD-mix  
**Short Description**: Character images with a specific set of nuisance parameters  
**\# Classes**: 706  
**\# Images**: 28240  
**Keywords**: ocr  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: CC BY 4.0  
**License URL(original data release)**: https://creativecommons.org/licenses/by/4.0/
 
**License (Meta-Album data release)**: CC BY 4.0  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/)  

**Source**: OmniPrint  
**Source URL**: https://github.com/SunHaozhe/OmniPrint  
  
**Original Author**: Haozhe Sun  
**Original contact**: sunhaozhe275940200@gmail.com  

**Meta Album author**: Haozhe Sun  
**Created Date**: 25 June 2021  
**Contact Name**: Haozhe Sun  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@inproceedings{sun2021omniprint,
    title={OmniPrint: A Configurable Printed Character Synthesizer},
    author={Haozhe Sun and Wei-Wei Tu and Isabelle M Guyon},
    booktitle={Thirty-fifth Conference on Neural Information Processing Systems Datasets and Benchmarks Track (Round 1)},
    year={2021},
    url={https://openreview.net/forum?id=R07XwJPmgpl}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44243)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44287) of an [OpenML dataset](https://www.openml.org/d/44287). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44287/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44287/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44287/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

